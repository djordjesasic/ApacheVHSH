#!/usr/bin/env bash

#THIS SCRIPT WILL REQUIRE ROOT PRIVILEGES
#RUN CAREFULY
#SCRIPT WILL PROPMT YOU FOR INSTALLING APACHE
#make sure update your system before runing this script
#make sure run this script as root
#you should configure /etc/hosts on your own.

set -e

if [[ $EUID -ne 0 ]]; then
	echo "make sure run as root" 
	exit 1
fi


echo "This is script for automatic setting up apache virtual host."
sleep 2
echo "If you don't have apache installed script will prompt you"
sleep 2
echo "Starting"
sleep 2

#using awk to find out user's distro
OS=`awk '/NAME=/' /etc/os-release | tr '[:upper:]' \
'[:lower:]' | grep -o 'name=[^ ,]\+'`

input=false

if [[ $OS == *"ubuntu"* ]]; then
	distro="Ubuntu"
elif [[ $OS == *"debian"* ]]; then
	distro="Debian"
elif [[ $OS == *"trios"* ]]; then
	distro="Trios"
elif [[ $OS == *"centos"* ]]; then
	distro="CentOS"
else
	echo "This script works only on Ubuntu, Debian, CentOS."
	exit 1
fi

InstallApache(){

	if [[ $distro == "Ubuntu" ]]; then
		sudo apt install apache2
		service apache2 start
	fi
	if [[ $distro == "Debian" ]]; then
		apt install apache2	
		service apache2 start
	fi
	if [[ $distro == "CenOS" ]]; then
		yum install httpd
		systemctl start httpd
		systemctl enable httpd.service
	fi

}

if [[ "$(which apache2)" == "" ]]; then
	echo "apache2 is not installed on your $distro."
	sleep 2
	while !(( input )); do
		read -p "do you want to install apache2? [Y/n]: " answer
	
		if [[ $answer == "y" || $answer == "" ]]; then
			InstallApache
			input=true
			break
		elif [[ $answer == "n" ]]; then
			echo "Fine, your choice"
			echo "Bye"
			exit
		
		else
			echo "wrong"
		fi
	done
else
	echo "apache2 is installed on your $distro."
	sleep 2
fi

echo "STARTING APACHE CONFIGURATION"
echo
sleep 2
echo "Enter site name: "
read siteName
echo "Site name is $siteName."
echo "Enter server admin: "
read serverAdmin
serverAlias="www.$siteName.com"
echo "Default site alias is $siteAlias"

mkdir -p /var/www/$siteName
DocRoot="/var/www/$siteName"

echo "Your site's folder is $DocRoot."
chown -R $USER:$USER $DocRoot
chmod 755 $DocRoot

if [[ $distro == "Ubuntu" || $distro == "Debian" || $distro == "Trios" ]]; then
	
	echo "
       		<VirtualHost *:80>
			ServerAdmin $serverAdmin@$siteName
			ServerName $siteName
			ServerAlias $serverAlias
			DocumentRoot $DocRoot
		</VirtualHost>\
		" >> /etc/apache2/sites-available/$siteName.conf
	a2ensite $siteName.conf
	a2dissite 000-default.conf
	systemctl restart apache2
	systemctl reload apache2

fi

if [[ $distro == "CentOS" ]]; then

	mkdir /etc/httpd/sites-available
	mkdir /etc/httpd/sites-enabled

	echo "
		<VirtualHost *:80>
			ServerAdmin $serverAdmin@$siteName
			ServerName $siteName
			ServerAlias $serverAlias
			DocumentRoot $DocRoot
		</VirtualHost>\n
	" >> /etc/httpd/sites-available/$siteName.conf
	ln -s /etc/httpd/sites-available/$siteName.conf /etc/httpd/conf.d/$siteName.conf	
	systemctl restart httpd.service	
fi

echo "Done."
sleep 3
echo "Script has finished its job."
sleep 3
echo "You should configure /etc/hosts on your own!"
sleep 3
echo "WARNING"
echo "Your site is set up successfuly."
sleep 3
echo "Thank you for testing ApacheSetupScript."
